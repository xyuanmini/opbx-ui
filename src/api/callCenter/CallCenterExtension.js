import request from '@/utils/request'

// 查询分机设置列表
export function listCallCenterExtension(query) {
  return request({
    url: '/callCenter/CallCenterExtension/list',
    method: 'get',
    params: query
  })
}

// 查询分机设置详细
export function getCallCenterExtension(id) {
  return request({
    url: '/callCenter/CallCenterExtension/' + id,
    method: 'get'
  })
}

// 新增分机设置
export function addCallCenterExtension(data) {
  return request({
    url: '/callCenter/CallCenterExtension',
    method: 'post',
    data: data
  })
}

// 修改分机设置
export function updateCallCenterExtension(data) {
  return request({
    url: '/callCenter/CallCenterExtension',
    method: 'put',
    data: data
  })
}

// 删除分机设置
export function delCallCenterExtension(id) {
  return request({
    url: '/callCenter/CallCenterExtension/' + id,
    method: 'delete'
  })
}

//改变分机状态
export function changeExtensionStatus(id, registerStatus) {
  const data = {
    id,
    registerStatus
  }
  return request({
    url: '/callCenter/CallCenterExtension/changeExtensionStatus',
    method: 'post',
    data: data
  })
}
